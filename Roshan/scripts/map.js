	// Google Map Customized

	var mymap = new google.maps.LatLng(34.555647, 69.203173);

	function initialize() {
		var customMapType = new google.maps.StyledMapType(
			[
				{
					"featureType": "all",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"saturation": 36
						},
						{
							"color": "#333333"
						},
						{
							"lightness": 40
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#fefefe"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#fefefe"
						},
						{
							"lightness": 17
						},
						{
							"weight": 1.2
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#dedede"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 29
						},
						{
							"weight": 0.2
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 18
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f2f2f2"
						},
						{
							"lightness": 19
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e9e9e9"
						},
						{
							"lightness": 17
						}
					]
				}

			], {
				name: 'Roshan Style'
			});
		var customMapTypeId = 'custom_style';
		var mapProp = {
			center:mymap,
			zoom:14,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
			}
		};

		// Set Custome Marker

		var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

		var marker= new google.maps.Marker({
			position:mymap,
			icon:'/Roshan/media/1104/pinkball.png'
		});

		// Set Custome Infowindow

		var custom_window = '<div class="infowin-outer">' +
								'<div class="mp-iw-itx">' +
									'<span class="win-ttl color text-uppercase">' +
										'Our Locations' +
									'</span>' +
									'<address class="addr-main">' +
										'Kabul Walk-in Customer Care Center <br />' +
										'Roshan Shop Street #13, Off Main Street <br />' +
										'Wazir Akbar Khan <br />' +
										'<b>Operating hours: 8 am - 5 pm</b>' +
									'</address>' +
								'</div>' +

								'<div class="mp-iw-itx">' +
									'<span class="win-ttl color text-uppercase">' +
										'Call Us' +
									'</span>' +
									'<address class="addr-main">' +
										'<b>For Roshan Inquriries, dial</b> <br />' +
										'333 from a Roshan Line Locally <br />' +
										'079 997 1333 from another national network locally <br />' +
										'+93 (0) 79 997 1333 from overseas <br />' +
									'</address>' +
								'</div>' +

								'<div>' +
									'<span class="win-ttl color text-uppercase">' +
										'E-Mail Us' +
									'</span>' +
									'<address class="addr-main">' +
										'<b>roshanca@roshan.af</b>' +
									'</address>' +
								'</div>' +
							'</div>';

		var infowindow = new google.maps.InfoWindow({
			content:custom_window,
			// pixelOffset: new google.maps.Size(100,100)
		});

		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});

		// use this if required onload show
		// infowindow.open(map,marker);

		marker.setMap(map);

		map.mapTypes.set(customMapTypeId, customMapType);
		map.setMapTypeId(customMapTypeId);
	}
	google.maps.event.addDomListener(window, 'load', initialize);