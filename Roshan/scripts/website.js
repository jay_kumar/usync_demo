// Your Scripts Here

$(document).ready(function(){
	$("#desk-srch, #mob-srch").on('click', function() {
		$(this).find('.fa-search').toggle();
		$(this).find('.cls-sub-box').toggle();
		$("#search-box").slideToggle();
		$("#mob-menu-trigger").find('.cls-sub-box-mm').hide();
		$("#mob-menu-trigger").find('.fa-bars').show();
		// $("#search-box").toggleClass('animated slideInDown');
	});

	$("#desk-srch, #mob-srch").on('click', function() {
		$("#subscribe-box").hide();
	});

	$("#sub-des, #sub-mob").on('click', function() {
		$("#search-box").hide();
		$("#desk-srch").find('.cls-sub-box').hide();
		$("#desk-srch").find('.fa-search').show();
		$("#mob-srch").find('.fa-search').show();
		$("#mob-srch").find('.cls-sub-box').hide();
		$("#mob-menu-trigger").find('.cls-sub-box-mm').hide();
		$("#mob-menu-trigger").find('.fa-bars').show();
	});

	$("#cls-commn-sub, #cls-commn-sub-des").on('click', function() {
		$("#subscribe-box").slideToggle();
	});

	$("#mob-menu-trigger").on('click', function() {
		$(this).find('.fa-bars').toggle();
		$(this).find('.cls-sub-box-mm').toggle();
	});

	$("#mob-menu-trigger").on('click', function() {
		$("#subscribe-box").hide();
		$("#search-box").hide();
		$("#mob-srch").find('.fa-search').show();
		$("#mob-srch").find('.cls-sub-box').hide();
	});

	$("#sub-des, #sub-mob").on('click', function(event) {
		event.stopPropagation();
		$("#subscribe-box").slideToggle();
	});

	$("#subscribe-box").on('click', function(event) {
		event.stopPropagation();
	});

	$('html').on('click', function(event){
		$("#subscribe-box").hide();
	});

	$('.dropdown-btn').on('click', function(event) {
		event.stopPropagation();
		$('#' + $(this).attr('dropdown-target')).fadeToggle();//hide()//slideToggle()//fadeToggle()
	});

	$("#main-cl li a").on('click', function() {
		$(this).parent().parent().parent().fadeOut();
	});

	$('.load-more-outer').on('click', function(event) {
		event.stopPropagation();
	});

	$('html').on('click', function(){
		$('.load-more-outer').fadeOut();
	});

	var count = 7;
	$('#main-cl li:lt(' + count + ')').show();
	// $('#showLess').hide();

	$('#loadMore').click(function () {
		// $('#showLess').show();
		$('#main-cl').css({
			overflowY: 'scroll'
		});

		count = $('#main-cl li:visible').length;
		$('#main-cl li:lt(' + (count + 7) + ')').show();
		if (count + 7 >= $('#main-cl li').length) {
			$(this).hide();
		}
	});

	// $(function() {
	// 	$( '#dl-menu' ).dlmenu();
	// });

	$(function() {
		$( '#dl-menu' ).dlmenu({
			animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
		});
	});

});
